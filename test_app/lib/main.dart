import 'package:flutter/material.dart';
import 'package:test_app/screens/List.dart';
void main()=>runApp(MyApp());
class MyApp extends StatelessWidget {
  @override

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      theme: ThemeData(brightness: Brightness.dark,),
      debugShowCheckedModeBanner: false,
      home: PlayerList(),
    );
  }
}