class Players{
  int id;
  String name;
  Players({this.id,this.name});
  factory Players.fromJSON(Map<String,dynamic>parsedJSON){
    return Players(id: parsedJSON["id"],name: parsedJSON["name"]);
  }
}