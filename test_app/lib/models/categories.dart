class Categories{
  int id;
  String name;
  Categories({this.id,this.name});
  factory Categories.fromJSON(Map<String,dynamic> parsedJson){
    return Categories(
      id: parsedJson['id'],
      name: parsedJson['name']
    );
  }
}