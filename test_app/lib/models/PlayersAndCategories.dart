class PlayersAndCategories{
  int playerID;
  List<dynamic> categories;
  List<bool> checkBoxes;
  PlayersAndCategories({this.categories,this.playerID});
  factory PlayersAndCategories.fromJSON(Map<String,dynamic>parsedJson){
    return PlayersAndCategories(
      categories: parsedJson['categoryIDs'],
      playerID: parsedJson['playerID']
    );
  }
}