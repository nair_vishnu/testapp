import 'package:async/async.dart';
import 'package:flutter/material.dart';
import 'package:test_app/presenters/tournament_player_presenter.dart';

class PlayerList extends StatefulWidget {
  @override
  _PlayerListState createState() => _PlayerListState();
}

class _PlayerListState extends State<PlayerList> {
  Map _playerList = {};
  AsyncMemoizer memo = new AsyncMemoizer();
  Future<dynamic> getData() async {
    try {
      _playerList =
          (await new TournamentPlayerPresenter().getTournamentPlayers()).data;
      return _playerList;
    } catch (e) {
      throw e;
    }
  }

  //
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Test App"),
      ),
      body: FutureBuilder(
        builder: (context, snap) {
          try {
            if (snap.connectionState == ConnectionState.done && snap.hasData) {
              return ListView.builder(
                itemCount: snap.data['tournamentPlayers'].length,
                itemBuilder: (context, index) {
                  return Card(
                    color: Colors.black,
                    child: CheckboxListTile(
                      activeColor: Colors.blue,
                      value: _playerList['tournamentPlayers'][index]['checked'],
                      onChanged: (val) {
                        setState(() {
                          _playerList['tournamentPlayers'][index]['checked'] =
                              val;
                           
                        });
                        if(!val){
                          _playerList['tournamentPlayers'][index]['categories'].forEach(m=>m.);
                        }
                      },
                      isThreeLine: true,
                      selected: _playerList['tournamentPlayers'][index]['checked'],
                      title: Text(
                        _playerList['tournamentPlayers'][index]['name'],
                        style: TextStyle(fontSize: 25),
                      ),
                      secondary: Icon(Icons.account_circle),
                      subtitle: Container(
                        height: 300,
                        child: ListView(
                          physics: const NeverScrollableScrollPhysics(),
                          padding: EdgeInsets.all(10),
                          children: <Widget>[
                            for (var item in _playerList['categories']) ...[
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: CheckboxListTile(
                                  selected: _playerList['tournamentPlayers'][index]
                                            ['categories'][
                                        _playerList['categories']
                                            .indexOf(item)]['checked'],
                                  activeColor: Colors.blue,
                                    title: Text(item.name),
                                    value: _playerList['tournamentPlayers'][index]
                                            ['categories'][
                                        _playerList['categories']
                                            .indexOf(item)]['checked'],
                                    onChanged: (val) {
                                      setState(() {
                                        _playerList['tournamentPlayers'][index]
                                                ['categories'][
                                            _playerList['categories']
                                                .indexOf(item)]['checked'] = val;
                                      });
                                    }),
                              )
                            ]
                          ],
                        ),
                      ),
                    ),
                  );
                },
              );
            } else if (snap.connectionState == ConnectionState.waiting) {
              return Center(child: CircularProgressIndicator());
            } else {
              return Center(
                child: Text("Something went wrong"),
              );
            }
          } catch (e) {
            throw e;
          }
        },
        future: memo.runOnce(() async => await getData()),
      ),
    );
  }
}
