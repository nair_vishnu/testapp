import 'dart:convert';
import 'package:test_app/models/PlayersAndCategories.dart';
import 'package:test_app/models/categories.dart';
import 'package:test_app/models/players.dart';
import 'package:test_app/models/result_class.dart';
import 'package:test_app/servies/category_service.dart';
import 'package:test_app/servies/player_service.dart';
import 'package:test_app/servies/tournaments_player_service.dart';

class TournamentPlayerPresenter {
  Future<ResultClass> getTournamentPlayers() async {
    try{
      var data = new List<dynamic>();

    var players = await new PlayerService().getPlayers();
    var tournamentPlayers = await new TournamentPlayerService().getPlayers();
    var categories = await new CategoryService().getCategories();

    var parsedTournamentPlayers = json.decode(tournamentPlayers.body);
    var parsedPlayers = json.decode(players.body);
    var parsedCategories = json.decode(categories.body);

    var listOfPlayersAndTheirCategories = new List<PlayersAndCategories>();
    var listOfCategories = new List<Categories>();
    var listOfPlayers = new List<Players>();

    var checkList = new List<Map>();
    if (parsedPlayers != null)
      for (var player in parsedPlayers) {
        listOfPlayers.add(Players.fromJSON(player));
      }
    if (parsedTournamentPlayers != null)
      for (var player in parsedTournamentPlayers) {
        listOfPlayersAndTheirCategories
            .add(PlayersAndCategories.fromJSON(player));
      }
    if (parsedCategories != null)
      for (var cat in parsedCategories) {
        listOfCategories.add(Categories.fromJSON(cat));
      }
    for (var item in listOfPlayers) {
      checkList = new List<Map>();
      // var thatPlayer = listOfPlayersAndTheirCategories
      //     .where((f) => f.playerID == item.id)
      if (listOfPlayersAndTheirCategories.firstWhere((f)=>f.playerID == item.id,orElse: ()=>null)!=null) {
        for (var category in listOfCategories) {
          checkList.add({
            'id': category.id,
            'checked': listOfPlayersAndTheirCategories
                .where((f) => f.playerID == item.id)
                .first
                .categories
                .contains(category.id)
          });
        }
        data.add({
          'id': item.id,
          'name': item.name,
          'checked': true,
          'categories': checkList
        });
      } else {
        for (var category in listOfCategories) {
          checkList.add({'id': category.id, 'checked': false});
        }
        data.add({
          'id': item.id,
          'name': item.name,
          'checked': false,
          'categories': checkList
        });
      }
    }
    return ResultClass(
        data: {'categories': listOfCategories, 'tournamentPlayers': data},
        message: "Data retrieved",
        resultCode: 1);
    }
    catch(e){
      throw e;
    }
  }
}
